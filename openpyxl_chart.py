from openpyxl import load_workbook
from openpyxl.chart import PieChart, Reference
from openpyxl.chart.series import DataPoint
from openpyxl.chart.label import DataLabelList

# data = [
#     ['Pie', 'Sold'],
#     ['Apple', 50],
#     ['Cherry', 30],
#     ['Pumpkin', 10],
#     ['Chocolate', 40],
# ]
workbook = load_workbook(filename = 'cpjg.xlsx')
ws = workbook.active

# for row in data:
#     ws.append(row)

pie = PieChart()

labels = Reference(ws, min_col=1, min_row=2, max_row=5)
data = Reference(ws, min_col=2, min_row=1, max_row=5)
pie.add_data(data, titles_from_data=True)
pie.set_categories(labels)
pie.title = "各词性百分比"
pie.dataLabels = DataLabelList() 
pie.dataLabels.showVal = True
pie.dataLabels.showCatName = True

pie.dataLabels.showSerName = True
pie.dataLabels.showPercent = True

slice = DataPoint(idx=0, explosion=20)
pie.series[0].data_points = [slice]
ws.add_chart(pie, "D1")


workbook.save('cpjg.xlsx')


# ----------------------------------------------------
# https://www.osgeo.cn/openpyxl/charts/radar.html
# 用雷达图对单词的范围进行评定
from openpyxl import Workbook
from openpyxl.chart import (
    RadarChart,
    Reference,
)

wb = Workbook()
ws = wb.active

rows = [
    ['词汇', "Bulbs"],
    ['四级', 23],
    ['六级', 53],
    ['考研', 223],
    ['雅思', 3],
    ['托福', 43]
]

for row in rows:
    ws.append(row)

chart = RadarChart()
chart.type = "filled"
labels = Reference(ws, min_col=1, min_row=2, max_row=13)
data = Reference(ws, min_col=2, max_col=5, min_row=1, max_row=13)
chart.add_data(data, titles_from_data=True)
chart.set_categories(labels)
chart.style = 1
chart.title = "Garden Centre Sales"
chart.y_axis.delete = True

ws.add_chart(chart, "A17")

wb.save("radar.xlsx")